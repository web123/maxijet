<?php

if(!function_exists('qode_add_slider1_shortcodes')) {
	function qode_add_slider1_shortcodes($shortcodes_class_name) {
		$shortcodes = array(
			'qodeNews\CPT\Shortcodes\Slider1\Slider1'
		);
		
		$shortcodes_class_name = array_merge($shortcodes_class_name, $shortcodes);
		
		return $shortcodes_class_name;
	}
	
	add_filter('qode_news_filter_add_vc_shortcode', 'qode_add_slider1_shortcodes');
}

if( !function_exists('qode_set_slider1_icon_class_name_for_vc_shortcodes') ) {
	/**
	 * Function that set custom icon class name for slider 1 shortcode to set our icon for Visual Composer shortcodes panel
	 */
	function qode_set_slider1_icon_class_name_for_vc_shortcodes($shortcodes_icon_class_array) {
		$shortcodes_icon_class_array[] = '.icon-wpb-post-slider1';
		
		return $shortcodes_icon_class_array;
	}
	
	add_filter('qode_news_filter_add_vc_shortcodes_custom_icon_class', 'qode_set_slider1_icon_class_name_for_vc_shortcodes');
}