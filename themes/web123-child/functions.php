<?php
/**
 *	Web123 WordPress Theme
 *
 */

//SCSS loader - once live change to FALSE
define('WP_SCSS_ALWAYS_RECOMPILE', true);


// This will enqueue style.css of child theme
function enqueue_childtheme_scripts() {
	wp_enqueue_style( 'web123-child', get_stylesheet_directory_uri() . '/style.css', '1.0' );
	wp_enqueue_script( 'web123', esc_url( trailingslashit( get_stylesheet_directory_uri() ) . 'js/web123-child.min.js' ) );
}
add_action( 'wp_enqueue_scripts', 'enqueue_childtheme_scripts', 100 );


// Logo on login screen
function custom_login_logo() {
    echo '<style type="text/css">
        h1 a { background-image:url(https://www.web123.com.au/wp-content/uploads/2016/09/logo-retina.png) !important; }
				.login h1 a { width: 100%!important; background-size: 160px!important; height: 200px!important;}
				body {background-color: #000!important; }
    </style>';
}

add_action('login_head', 'custom_login_logo');


/* Hide WP version strings from scripts and styles
 * @return {string} $src
 * @filter script_loader_src
 * @filter style_loader_src
 */
function remove_wp_version_strings( $src ) {
     global $wp_version;
     parse_str(parse_url($src, PHP_URL_QUERY), $query);
     if ( !empty($query['ver']) && $query['ver'] === $wp_version ) {
          $src = remove_query_arg('ver', $src);
     }
     return $src;
}
add_filter( 'script_loader_src', 'remove_wp_version_strings' );
add_filter( 'style_loader_src', 'remove_wp_version_strings' );

/* Hide WP version strings from generator meta tag */
function wpmudev_remove_version() {
return '';
}
add_filter('the_generator', 'wpmudev_remove_version');


//Devshop - local dev URL helper
function site_base_url(){
	$base_url = get_site_url();
 		return $base_url;
}

add_shortcode('BU','site_base_url');

/* Product custom post type*/


function custom_post_type0() {

// Set UI labels for Custom Post Type
	$labels = array(
		'name'                => _x( 'Product', 'Post Type General Name', 'twentythirteen' ),
		'singular_name'       => _x( 'Products', 'Post Type Singular Name', 'twentythirteen' ),
		'menu_name'           => __( 'Product', 'twentythirteen' ),
		'parent_item_colon'   => __( 'Parent Product', 'twentythirteen' ),
		'all_items'           => __( 'All Product', 'twentythirteen' ),
		'view_item'           => __( 'View Product', 'twentythirteen' ),
		'add_new_item'        => __( 'Add New Product', 'twentythirteen' ),
		'add_new'             => __( 'Add Product', 'twentythirteen' ),
		'edit_item'           => __( 'Edit Product', 'twentythirteen' ),
		'update_item'         => __( 'Update Product', 'twentythirteen' ),
		'search_items'        => __( 'Search Product', 'twentythirteen' ),
		'not_found'           => __( 'Not Found', 'twentythirteen' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'twentythirteen' ),
	);
	
// Set other options for Custom Post Type
	
	$args = array(
		'label'               => __( 'Product', 'twentythirteen' ),
		'description'         => __( 'Product and Product reviews', 'twentythirteen' ),
		'labels'              => $labels,
		// Features this CPT supports in Post Editor
		'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
		// You can associate this CPT with a taxonomy or custom taxonomy. 
		'taxonomies'          => array( 'Product_type' ),
		/* A hierarchical CPT is like Pages and can have
		* Parent and child items. A non-hierarchical CPT
		* is like Posts.
		*/	
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'page',
	);
	
	// Registering your Custom Post Type
	register_post_type( 'Products', $args );

}


add_action( 'init', 'custom_post_type0', 0 );

/*taxonomies code*/

//hook into the init action and call create_topics_nonhierarchical_taxonomy when it fires

add_action( 'init', 'create_topics_nonhierarchical_taxonomy', 0 );

function create_topics_nonhierarchical_taxonomy() {

// Labels part for the GUI

 $labels = array(
   'name' => _x( 'Categories', 'taxonomy general name' ),
   'singular_name' => _x( 'Categories', 'taxonomy singular name' ),
   'search_items' =>  __( 'Search Categories' ),
   'popular_items' => __( 'Popular Categories' ),
   'all_items' => __( 'All Categories' ),
   'parent_item' => null,
   'parent_item_colon' => null,
   'edit_item' => __( 'Edit Categories' ), 
   'update_item' => __( 'Update Categories' ),
   'add_new_item' => __( 'Add New Categories' ),
   'new_item_name' => __( 'New Categories Name' ),
   'separate_items_with_commas' => __( 'Separate Categories with commas' ),
   'add_or_remove_items' => __( 'Add or remove Categories' ),
   'choose_from_most_used' => __( 'Choose from the most used Categories' ),
   'menu_name' => __( 'Categories' ),
 ); 

// Now register the non-hierarchical taxonomy like tag

 register_taxonomy('Categories','products',array(
   'hierarchical' => true,
   'labels' => $labels,
   'show_ui' => true,
   'show_admin_column' => true,
   'update_count_callback' => '_update_post_term_count',
   'query_var' => true,
   'rewrite' => array( 'slug' => 'Categories' ),
 ));
}

